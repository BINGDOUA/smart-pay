package com.founder.service;

import com.founder.core.domain.MchInfo;

public interface IMchInfoService {

    /**
     * 按照商户号查询
     * @param mchId
     * @return
     */
    MchInfo selectMchInfo(String mchId);
}
