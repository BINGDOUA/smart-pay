package com.founder.service.impl;

import com.founder.core.dao.PayChannelRespository;
import com.founder.core.domain.PayChannel;
import com.founder.service.IPayChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PayChannelServiceImpl implements IPayChannelService {

    @Autowired
    PayChannelRespository payChannelRespository;

    @Override
    public PayChannel selectPayChannel(String channelId, String mchId) {
        return payChannelRespository.findByMchIdAndChannelId(mchId,channelId);
    }
}
